cd tests
cd 1-*
rm rise2-translated.cpp
rm rise2-app
rm rise2-depencencies.hpp
python ~/Sources/my/rise2beckett/rise2beckett.py .
python ../../rise2.py main.ri2 rise2-translated.cpp "<NONE>"
clang++ -std=c++20 rise2-translated.cpp -o rise2-app
chmod +x rise2-app
./rise2-app
cd ..

cd 2-*
rm rise2-translated.cpp
rm rise2-app
rm rise2-depencencies.hpp
python ~/Sources/my/rise2beckett/rise2beckett.py .
python ../../rise2.py main.ri2 rise2-translated.cpp "<NONE>"
clang++ -std=c++20 rise2-translated.cpp -o rise2-app
chmod +x rise2-app
./rise2-app
cd ..

cd 3-*
rm rise2-translated.cpp
rm rise2-app
rm rise2-depencencies.hpp
python ~/Sources/my/rise2beckett/rise2beckett.py .
python ../../rise2.py main.ri2 rise2-translated.cpp "<NONE>"
clang++ -std=c++20 rise2-translated.cpp -lSDL2 -lGLESv2 -o rise2-app
chmod +x rise2-app
./rise2-app
cd ..