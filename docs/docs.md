Rise 2 file extension - ri2

Rise 2 *only* code to code translator

Dependencies are privilege of other tools, for example - Rise 2 Beckett tool.
Rise 2 does not support any *use* or *include* or *import* in syntax, because it makes source code less readable and very noisy.

Also *no* namespaces or similar

Tools roles:

- Rise2 - only works with source code
- Jabulon - builds Rise2 projects
- Beckett - generates dependincies

Single responsibility per tool

Code style:

- CamelCase

- Class name - first letter always uppercased

- Method name - first letter always downcased

- Only tabs (4 spaces)

- closing } only one and on newline

- opening { only at same line with if/while/for etc.

- only three tabs deep supports (decompose more) (!Check!)

- NO semicolons!!!

To compile: python3 rise2.py helloWorld.ri2

Syntax:

- New line for each language sentence

- Classes:

ClassName {
}

- Methods:

methodName {
}

methodName(varible: Type) {
}

methodName(variable: Type, variable2: Type) = ReturnType {
}

- Return:
    rt someValue
    last line always returns


- Contracts:

contractMethod

contractMethod(variable: Type)

contractMethod(variable: Type, variable2: Type) = ReturnType

- Visibility:

public
    * everything after that will be public

insider
    * everything after that will be protected

private
    * everything after that will be private

- Generics:

<T: Type, K: Type>
    * everyhting after that will work with T and K as types.

Restrictions:

    - only one class per file
    - tuples does not supported

All lines that was not understood by translator will be passed as is.